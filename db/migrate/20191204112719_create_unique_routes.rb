class CreateUniqueRoutes < ActiveRecord::Migration[6.0]
  def change
    create_table :unique_routes do |t|

      t.timestamps
    end
  end
end
