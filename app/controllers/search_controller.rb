class SearchController < ApplicationController
  def search
    if params[:q].nil?
      @unique_routes = []
    else
      @unique_routes = UniqueRoute.search params[:q]
    end
  end
end
