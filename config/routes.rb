Rails.application.routes.draw do
  resources :books
  devise_for :users
  root to: "books#index"
    get '/*a', to: 'application#not_found'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
